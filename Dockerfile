FROM telegraf:alpine

RUN apk add --no-cache fail2ban

# Allow access to Docker Socket
USER root
This image is a clone of the Alpine version of `telegraf:alpine`  but provides also fail2ban to allow the usage of fail2ban plugin.

**This image runs as ROOT**

You are warned, but you may override it if you do not need to access docker socket, or you can use specific techniques to change permissions. 

## Volumes

You need to mount those two volumes : 
* `/etc/fail2ban:/etc/fail2ban`
* `/var/run/fail2ban:/var/run/fail2ban`
